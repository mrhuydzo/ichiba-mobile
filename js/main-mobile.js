"use strict"

let tabs = (item,content_first,content) => {
    content_first.addClass('active');
    item.click(function () {
       item.removeClass('active');
       $(this).addClass('active');
       let cur = $(this).index();

       content.removeClass('active');
        //console.log(content.eq(cur).addClass('active'));
       content.eq(cur).addClass('active');
    });
};

//Preloader
let preloader = () =>  {
    let preloader = $('.preloader');
    $(window).on('load', function() {
        preloader.fadeOut(800);
        //AOS.init();
    });
};

let bannerSlide = () => {
    $('.banner_slide').slick({
        arrows: false,
        dots: true,
    });
};

let closeMenuMobile = () => {
    /* When user clicks outside */
    $(".menu_mobile_close").click(function() {
        $(this).removeClass('open');
        $('.bg_overlay').removeClass('open');
        $(".menu_mobile").removeClass("open");
        $('html , body').removeClass('open_menu');
    });

    $('.bg_overlay').click(function () {
       $(this).removeClass('open');
        $(".menu_mobile").removeClass("open");
        $('html , body').removeClass('open_menu');
    });
};

let toggleHamburgerIcon  = () => {
    $('.header_hamburger_icon').click(function(){
        $(this).addClass('open');
        $('.menu_mobile_left').addClass('open');
        $('.bg_overlay').addClass('open');
        $('html , body').addClass('open_menu');
    });
};

let toggleSubmenu = () => {
  let arrButton = $ ('.menu_mobile_arr');
  let backButton = $ ('.menu_mobile_back');
    arrButton.click(function () {
        $(this).next().addClass('open');
        /*$('.menu_mobile_sub').css('height','auto');
        $(this).next().css('height','100%');*/
    });
    backButton.click(function () {
        //console.log($(this).offsetParent());
       $(this).offsetParent().removeClass('open');
        //$('.menu_mobile_sub').css('height','100%');
    });
};

let toggleUserIcon  = () => {
    $('.header_user_icon').click(function(){
        $(this).addClass('open');
        $('.menu_mobile_right').addClass('open');
        $('.bg_overlay').addClass('open');
        $('html , body').addClass('open_menu');
    });
};

let tabStore = () => {
    let item = $('.store_tab .tab_item');
    let content = $('.store_tab .tab_content_item');
    let content_first = $('.store_tab .tab_content_item:first-child');
    tabs(item,content_first,content);
};

let tabSuggest = () => {
    let item = $('#tab_suggest .tab_item');
    let content = $('#tab_suggest .tab_content_item');
    let content_first = $('#tab_suggest .tab_content_item:first-child');
    tabs(item,content_first,content);
};

let tabProductMain = () => {
    let item = $('.product_tab_item');
    let content = $('.product_main_tab .tab_content_item');
    let content_first = $('.product_main_tab .tab_content_item:first-child');
    tabs(item,content_first,content);
};

let openOptionsPage = () => {
    $('.block_header_arrow').click(function () {
        $(this).toggleClass('active');

        $(document).mouseup(function (e) {
            if (!$(e.target).hasClass("block_header_arrow") && $(e.target).parents(".block_header_arrow").length === 0) {
                $(".block_header_arrow").removeClass('active');
            }
        });
    });
};

let galleryProduct = () => {
    $('.gallery_full').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });
};

let toggleHeaderSelect = () => {
    let headerSelect = $('.header_select');
    let headerSelectOpen = $('.header_select.open');
    headerSelect.click(function () {
        $(this).toggleClass('open');
        $(this).siblings().removeClass('open');
    });
    headerSelectOpen.click(function () {
//        console.log('aa');
    });
};

let toggleFilter = () => {
  let headerFilter = $('.filter_header');
  headerFilter.click(function () {
     $(this).parent().toggleClass('close');
  });
};
let openPopupSearch = () => {
    $('.header_search').click(function () {
        $('.popup_container').addClass('open');
        $('.header_search_input').focus();
        $('html,body').css ('overflow','hidden');
    });
};
let closePopupSearch = () => {
    $('.button_close_popup').click(function () {
        $('.popup_container').removeClass('open');
        $('html,body').css ('overflow','auto');
    });
};
let openHeaderSearch = () => {
    $('.header_button_search').click(function () {
        $('.popup_container').addClass('open');
        $('html,body').css ('overflow','hidden');
    });
};


let quantiyCountBox = () => {
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }
    $('.button_plus').click(function (e) {
        incrementValue(e);
    });
    $('.button_minus').click(function (e) {
        decrementValue(e);
    });
};

let togglePaymentMethod = () => {
    let item = $('.payment_type_selector--item');
    let method = $('.payment_type');
    method.hide();
    item.click(function () {

        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else {
            item.removeClass('active');
            $(this).addClass('active');
        }

        $(this).parent().siblings().find(method).slideUp();
        $(this).parent().find(method).slideToggle();

    });
};

let scrollFixNav = () => {
    $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 55) {
            $('#header').addClass('fixed');
            $('#main').css('padding-top','55px');
        }
        else if ($(window).scrollTop()  == 0) {
            $('#header').removeClass('fixed');
            $('#main').css('padding-top','0');
        }
    });
}

$(document).ready(function () {
    toggleHamburgerIcon();
    toggleUserIcon ();
    closeMenuMobile();
    bannerSlide();
    tabStore();
    tabSuggest ();
    openOptionsPage ();
    galleryProduct ();
    tabProductMain ();
    toggleSubmenu ();
    toggleHeaderSelect ();
    toggleFilter ();
    openPopupSearch();
    openHeaderSearch ();
    closePopupSearch ();
    quantiyCountBox ();
    togglePaymentMethod();
    scrollFixNav();
});